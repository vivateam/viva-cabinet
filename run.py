#!env/bin/python2.7
from app import app
from app.config import *

app.run(debug = True, host=FLASK_IP, port=FLASK_PORT)
