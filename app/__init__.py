# -*- coding: utf-8 -*-
from flask import Flask
import os
import re
from logger import logger
from flask_login import LoginManager
from flask_babel import Babel
from flask_babel import gettext
from flask_babel import lazy_gettext

def load_version():
    """ Завантажує номер кабінету з файлу app/../version.txt.

        Returns
        -------
            str Версію кабінету, або, у випадку невдачі, нічого (порожній результат).
    """

    version = ''

    version_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) # на 1 рівень вище від поточної директорії
    version_file = os.path.join(version_dir, 'version.txt')

    logger.debug('Шлях до version.txt: %s' %version_file)

    if os.path.isfile(version_file):
        try:
            with open(version_file, mode='r') as fp:
                for line in fp:
                    if not line.startswith("#"):
                        pattern = re.compile("^\d+\.\d+-\d+-\w{8}$") # 2.0-2-g60dc2ee
                        if pattern.match(line):
                            logger.info('Версія кабінету: %s' %line)
                            version = line
        except:
            logger.error('Сталася помилка при спробі прочитати версію проекту!')
    else:
        logger.error('Файл %s не знайдено!' %version_file)

    return version

app = Flask(__name__, static_folder='static')
app.config.from_object('app.config')

app.config['BABEL_DEFAULT_LOCALE'] = 'uk'
babel = Babel(app)

CABINET_VERSION = load_version()

from app import views

lm = LoginManager()
lm.init_app(app)
lm.login_view = 'login'
lm.login_message = lazy_gettext(u'Для доступу в Персональний кабінет необхідна авторизація')
lm.login_message_category = 'alert-error'

@lm.user_loader
def load_user(id_abonent):
    import auth
    return auth.User(id_abonent)

