# -*- coding: utf-8 -*-

import logging

from config import *

logger = logging.getLogger(ALIAS)
logger.setLevel(logging.DEBUG)

fh = logging.FileHandler(LOG_FILE)
fh.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s [%(name)s] [%(levelname)s] %(message)s')
fh.setFormatter(formatter)

formatter_ch = logging.Formatter('[%(levelname)s] %(message)s')
ch.setFormatter(formatter_ch)


logger.addHandler(fh)
logger.addHandler(ch)
