# -*- coding: utf-8 -*-

from flask_wtf import FlaskForm
from wtforms import TextField, TextAreaField, BooleanField, PasswordField, SelectField, validators
from wtforms.validators import Required

from flask import render_template, flash, redirect, request, url_for, session

from wtforms.fields.html5 import EmailField
from wtforms.fields.html5 import TelField

from datetime import datetime, date
from config import *
import json
import auth
from flask_login import login_user, logout_user, current_user, login_required
from flask_babel import gettext
from flask_babel import lazy_gettext


class FTS_City(FlaskForm):
    city = TextField('City', [validators.Length(min=4, max=25), validators.Required()])

class FTS_Street(FlaskForm):
    street = TextField('Street', [validators.Length(min=4, max=100), validators.Required()])

class FTS_Build(FlaskForm):
    build = TextField('Build', [validators.Length(min=1, max=10), validators.Required()])

class FTS_Apartment(FlaskForm):
    apartment = TextField('Float', [validators.Length(min=1, max=5), validators.Required()])

class FTS_ConnectApp(FlaskForm):
    address = TextField('Address')
    name = TextField('Name', [validators.Length(min=1, max=20), validators.Required()])
    phone = TelField('Phone', [validators.Required()])
    email = EmailField('Email address')
    want_internet = BooleanField('Callback', default = False)
    want_catv = BooleanField('Callback', default = False)
    wishes = TextAreaField('Wishes')

class LoginForm(FlaskForm):
  flogin = TextField('Login', [validators.Length(min=1, max=30)])
  fpassword = PasswordField('Password', [
  validators.Required()])

  def __init__(self, *args, **kwargs):
    FlaskForm.__init__(self, *args, **kwargs)
    self.user = None

  def validate(self):
    rv = FlaskForm.validate(self)
    if not rv:
      return False

    if self.flogin.data == self.fpassword.data:
      self.flogin.errors.append(lazy_gettext(u'Номер Договору не повинен дорівнювати паролю.'))

      # GET LOCAL STATUS
      is_local = auth.is_local(auth.ip_user())
      if is_local:
        self.flogin.errors.append(lazy_gettext(u'Виконайте вхід без авторизації.'))

      return False


    param = { 'ip_user': auth.ip_user(),
      'login': self.flogin.data,
      'password': self.fpassword.data,
      'auth': TYPE_AUTH,
      'key': CABINET_KEY }

    user = auth.get_cabinet('get_user',param)

    if user:
      self.user = user
      return True

    else:
      if TYPE_AUTH == 'num':
        self.flogin.errors.append(lazy_gettext(u'Невірний Номер Договору або пароль.'))
      if TYPE_AUTH == 'nick':
        self.flogin.errors.append(lazy_gettext(u'Невірний Логин або пароль.'))
      if TYPE_AUTH == 'phone':
        self.flogin.errors.append(lazy_gettext(u'Невірний Номер телефону або пароль.'))
      if TYPE_AUTH == 'email':
        self.flogin.errors.append(lazy_gettext(u'Невірна Електронна пошта або пароль.'))
      return False

    self.user = user
    return True


class PostconfirmForm(FlaskForm):

  password = PasswordField('Password', [validators.Length(min=4, max=25)])

  def __init__(self, *args, **kwargs):
    FlaskForm.__init__(self, *args, **kwargs)
    self.user = None

  def validate(self):
    rv = FlaskForm.validate(self)
    if not rv:
      return False

    user = current_user

    param = { 'id_abonent' : user.id_abonent,
              'password' : self.password.data
            }

    res_confirm = auth.get_cabinet('get_confirm',param)
    if res_confirm['confirmed'] == True:      
      session['confirmed'] = True
      flash(gettext(u'Авторизація успішна.'),'alert-success')
    else:
      self.password.errors.append(lazy_gettext(u'Невірний пароль.'))
      return False

    self.user = user
    return True


class ChangepassForm(FlaskForm):

  newpassword = PasswordField('New Password', [
    validators.Required(),
    validators.Length(min=6, max=25)
    ])

  confirm = PasswordField('Repeat Password')

  def __init__(self, *args, **kwargs):
    FlaskForm.__init__(self, *args, **kwargs)
    self.user = None

  def validate(self):
    rv = FlaskForm.validate(self)
    if not rv:
      return False

    if self.newpassword.data != self.confirm.data:
      self.newpassword.errors.append(lazy_gettext(u'Паролі повинні співпадати.'))
      return False

    user = current_user

    param = { 
              'id_abonent' : user.id_abonent,
              'new' : self.newpassword.data,
              'confirm' : self.confirm.data
            }

    res_changepass = auth.get_cabinet('set_password',param)

    if res_changepass['code'] == 2:
      self.newpassword.errors.append(gettext(u'Новий пароль не повинен дорівнювати номеру Договору.'))
      return False

    if res_changepass['code'] == 1:
      self.oldpassword.errors.append(gettext(u'Невірний старий пароль.'))
      return False

    if res_changepass['code'] == 0:
      session['confirmed'] = True
      flash(gettext(u'Пароль успішно замінено.'),'alert-success')
      return True

    self.user = user
    return True


class ScratchForm(FlaskForm):

  passwd = TextField('Passwd', [validators.Length(min=10, max=10), validators.Required()])

  def __init__(self, *args, **kwargs):
    FlaskForm.__init__(self, *args, **kwargs)
    self.user = None

  def validate(self):
    rv = FlaskForm.validate(self)
    if not rv:
      return False

    user = current_user

    param = { 
              'id_abonent' : user.id_abonent,
              'passwd' : self.passwd.data
            }

    res_scratch = auth.get_cabinet('set_scratch',param)

    if res_scratch['code'] == 6:
      self.passwd.errors.append(gettext(u'Термін дії картки закінчився.'))
      return False

    if res_scratch['code'] == 5:
      self.passwd.errors.append(gettext(u'Заблокована серія.'))
      return False

    if res_scratch['code'] == 4:
      self.passwd.errors.append(gettext(u'Серія не призначена для цього провайдера.'))
      return False

    if res_scratch['code'] == 3:
      self.passwd.errors.append(gettext(u'Картку було використано.'))
      return False

    if res_scratch['code'] == 2:
      self.passwd.errors.append(gettext(u'Неправильний код картки.'))
      return False

    if res_scratch['code'] == 1:
      self.passwd.errors.append(gettext(u'Провайдер не використовує картки.'))
      return False

    if res_scratch['code'] == 0:
      session['confirmed'] = True
      flash(gettext(u'Картку поповнення успішно використано.'),'alert-success')
      return True

    self.user = user
    return True


class NewdeviceForm(FlaskForm):

  agree = BooleanField('I agree', [validators.Required()])

  def __init__(self, *args, **kwargs):
    FlaskForm.__init__(self, *args, **kwargs)
    self.user = None

  def validate(self):
    rv = FlaskForm.validate(self)
    if not rv:
      return False

    user = current_user

    param = { 
              'id_abonent' : user.id_abonent,
              'ip_address' : auth.ip_user()
            }

    res_newdevice = auth.get_cabinet('set_status_unknown_action',param)

    if res_newdevice['code'] == 7:
      flash(gettext(u'Пристрій успішно підключено. Протягом однієї хвилини Ваш пристрій буде активовано в мережі.'),'alert-success')
      return False

    if res_newdevice['code'] == 6:
      flash(gettext(u'Підключення нового пристрою дозволено тільки з адреси, яка закріплена за Вашим Договором.'),'alert-error')
      return False

    if res_newdevice['code'] == 5:
      flash(gettext(u'Невідома помилка. Зверніться в службу технічної підтримки Вашого провайдера.'),'alert-error')
      return False

    if res_newdevice['code'] == 4:
      flash(gettext(u'Невідома помилка. Зверніться в службу технічної підтримки Вашого провайдера.'),'alert-error')
      return False

    if res_newdevice['code'] == 3:
      flash(gettext(u'За Вашим Договором закріплено декілька Тарифних планів. Скористайтесь меню Керування у відповідному Тарифному плані.'),'alert-error')
      return False

    if res_newdevice['code'] == 2:
      flash(gettext(u'Невідома помилка. Зверніться в службу технічної підтримки Вашого провайдера.'),'alert-error')
      return False

    if res_newdevice['code'] == 1:
      flash(gettext(u'Підключення нового пристрою дозволено тільки з адреси, яка закріплена за Вашим Договором.'),'alert-error')
      return False

    if res_newdevice['code'] == 0:
      flash(gettext(u'Пристрій успішно підключено. Протягом однієї хвилини Ваш пристрій буде активовано в мережі.'),'alert-success')
      return True

    self.user = user
    return True


class WpaymentForm(FlaskForm):

  agree = BooleanField('I agree', [validators.Required()])

  def __init__(self, *args, **kwargs):
    FlaskForm.__init__(self, *args, **kwargs)
    self.user = None

  def validate(self):
    rv = FlaskForm.validate(self)
    if not rv:
      return False

    user = current_user

# Agree with wait for payment
    param = { 'id_abonent' : user.id_abonent,
              'only_check' : False
            }

    res_wpayment = auth.get_cabinet('wpayment',param)
    if res_wpayment["code"] == 0:
      flash(res_wpayment['message'],'alert-success')
    else:
      #self.allow_inform.errors.append(res_callback['message'])
      return False

    self.user = user
    return True


class BonusForm(FlaskForm):

  auto = BooleanField('Auto bonus', default = False)

  def __init__(self, *args, **kwargs):
    FlaskForm.__init__(self, *args, **kwargs)
    self.user = None

  def validate(self):
    rv = FlaskForm.validate(self)
    if not rv:
      return False

    user = current_user

    # Agree with Auto bonus
    param = { 'id_abonent' : user.id_abonent,
              'auto' : self.auto.data
            }

    res_auto_bonus = auth.get_cabinet('set_auto_bonus',param)
    if res_auto_bonus["code"] == 0:
      flash(gettext(u'Дані успішно збережено.'),'alert-success')
    else:
      #self.allow_inform.errors.append(res_callback['message'])
      return False

    self.user = user
    return True


class ContactsForm(FlaskForm):
#  phone = TelField('Phone', [])
#  email = EmailField('Email address', [])
  phone1 = TelField('Phone', [validators.DataRequired(), validators.Length(min=10, max=10)])
  phone2 = TelField('Phone', [validators.Length(min=0, max=10)])
  email = EmailField('Email address', [validators.DataRequired(), validators.Email()])
  callback = BooleanField('Callback', default = False)
# Begin Personal section
# Date Birth
  date_birth = TextField()
# End Personal section

  def __init__(self, *args, **kwargs):
    FlaskForm.__init__(self, *args, **kwargs)
    self.user = None

  def validate(self):
    rv = FlaskForm.validate(self)
    if not rv:
      return False

    user = current_user

# phone 1
    param = { 'id_abonent' : user.id_abonent,
              'type' : 'phone1',
              'contact' : {
                'number' : self.phone1.data
                          }
            }
    res_phone = auth.get_cabinet('set_contacts',param)
    if res_phone["code"] > 0:
      self.phone1.errors.append(res_phone['message'])
      return False

# phone 2
    param = { 'id_abonent' : user.id_abonent,
              'type' : 'phone2',
              'contact' : {
                'number' : self.phone2.data
                          }
            }
    res_phone = auth.get_cabinet('set_contacts',param)
    if res_phone["code"] > 0:
      self.phone2.errors.append(res_phone['message'])
      return False

# Email
    param = { 'id_abonent' : user.id_abonent,
              'type' : 'email',
              'contact' : {
                'name' : self.email.data
                          }
            }
    res_email = auth.get_cabinet('set_contacts',param)
    if res_email["code"] > 0:
      self.email.errors.append(res_email['message'])
      return False

# Callback
    param = { 'id_abonent' : user.id_abonent,
              'type' : 'callback',
              'contact' : {
                'value' : self.callback.data
                          }
            }
    res_callback = auth.get_cabinet('set_contacts',param)
    if res_callback["code"] > 0:
      self.callback.errors.append(res_callback['message'])
      return False

    # Begin Personal section

    param = { 
              'id_abonent' : user.id_abonent,
            }
    allow_storage = auth.get_cabinet('get_allow_storage',param)

    if allow_storage['code'] == 0:

      # Date Birth
      param = { 
                'id_abonent' : user.id_abonent,
                'cod_typ_abonent_storage' : 1,
                'data' : self.date_birth.data
              }
      res_storage = auth.get_cabinet('set_abonent_storage',param)
      if res_callback["code"] == 1:
        self.callback.errors.append(u'Некоректна дата.')
        return False

    # End Personal section

    self.user = user

    flash(gettext(u'Контактні дані успішно збережено.'),'alert-success')

    return True


class LastPaymentForm(FlaskForm):
  combo_box = SelectField('Date', coerce=int)

class LastCostsForm(FlaskForm):
  combo_box = SelectField('Date', coerce=int)

class StatisticForm(FlaskForm):
  combo_box = SelectField('Date', coerce=int)


class QuestionCreateForm(FlaskForm):
  quest = TextAreaField('Wishes', [validators.Length(min=10, max=500), validators.Required()])
  phone = TelField('Phone', [validators.Required()])
  email = EmailField('Email address')

class QuestionCloseForm(FlaskForm):
  agree = BooleanField('I agree', [validators.Required()])


class SrvRealipOnForm(FlaskForm):
  agree = BooleanField('I agree', [validators.Required()])

class SrvRealipOffForm(FlaskForm):
  agree = BooleanField('I agree', [validators.Required()])


class SrvDisableForm(FlaskForm):
  agree = BooleanField('I agree', [validators.Required()])

class SrvEnableForm(FlaskForm):
  agree = BooleanField('I agree', [validators.Required()])

class SrvSheduleDisableForm(FlaskForm):
  agree = BooleanField('I agree', [validators.Required()])

class SrvSheduleEnableForm(FlaskForm):
  agree = BooleanField('I agree', [validators.Required()])

class SrvCancelSheduleDisableForm(FlaskForm):
  agree = BooleanField('I agree', [validators.Required()])

class SrvCancelSheduleEnableForm(FlaskForm):
  agree = BooleanField('I agree', [validators.Required()])

class SrvShedulePacketForm(FlaskForm):
  agree = BooleanField('I agree', [validators.Required()])
  packet = SelectField(lazy_gettext(u'Виберіть зі списку'), coerce=int)

class SrvChangePacketForm(FlaskForm):
  agree = BooleanField('I agree', [validators.Required()])
  packet = SelectField(lazy_gettext(u'Виберіть зі списку'), coerce=int)

class SrvCancelShedulePacketForm(FlaskForm):
  agree = BooleanField('I agree', [validators.Required()])

class SrvSheduleSpeedwayForm(FlaskForm):
  agree = BooleanField('I agree', [validators.Required()])
  speedway = SelectField('Speedway', coerce=int)

class SrvCancelSpeedwayForm(FlaskForm):
  agree = BooleanField('I agree', [validators.Required()])

class RememberPasswordBySMSForm(FlaskForm):
  agree = BooleanField('I agree', [validators.Required()])

class RememberPasswordByMailForm(FlaskForm):
  agree = BooleanField('I agree', [validators.Required()])

class MailInfoForm(FlaskForm):

  mail = BooleanField('Callback', default = False)

  def __init__(self, *args, **kwargs):
    FlaskForm.__init__(self, *args, **kwargs)
    self.user = None

  def validate(self):
    rv = FlaskForm.validate(self)
    if not rv:
      return False

    user = current_user

    param = { 'id_abonent' : user.id_abonent,
              'type' : 'mail',
              'value' : self.mail.data
            }
    res_callback = auth.get_cabinet('set_speed_info',param)
    if res_callback["code"] > 0:
      self.callback.errors.append(res_callback['message'])
      return False

    self.user = user

    if self.mail.data == True:
      flash(gettext(u'E-mail інформування увімкнено.'),'alert-success')
    else:
      flash(gettext(u'E-mail інформування вимкнено.'),'alert-success')

    return True


class SMSInfoForm(FlaskForm):

  sms = BooleanField('Callback', default = False)

  def __init__(self, *args, **kwargs):
    FlaskForm.__init__(self, *args, **kwargs)
    self.user = None

  def validate(self):
    rv = FlaskForm.validate(self)
    if not rv:
      return False

    user = current_user

    param = { 'id_abonent' : user.id_abonent,
              'type' : 'sms',
              'value' : self.sms.data
            }
    res_callback = auth.get_cabinet('set_speed_info',param)
    if res_callback["code"] > 0:
      self.callback.errors.append(res_callback['message'])
      return False

    self.user = user

    if self.sms.data == True:
      flash(gettext(u'SMS інформування увімкнено.'),'alert-success')
    else:
      flash(gettext(u'SMS інформування вимкнено.'),'alert-success')

    return True


class RecoveryFormMailOrder(FlaskForm):

  flogin = TextField('Login', [validators.Length(min=1, max=18)])
  email = EmailField('Email address', [validators.DataRequired(), validators.Email()])

  def __init__(self, *args, **kwargs):
    FlaskForm.__init__(self, *args, **kwargs)
    self.user = None

  def validate(self):
    rv = FlaskForm.validate(self)
    if not rv:
      return False

    param = { 
              'login' : self.flogin.data,
              'ident' : self.email.data,
              'key_personal_room' : CABINET_KEY,
              'action': 'mail'
            }

    res = auth.get_cabinet('set_recovery_password_order',param)

    if res['code'] == 3:
      self.flogin.errors.append(lazy_gettext(u'Перевірте правильність введених даних.'))
      return False

    if res['code'] == 2:
      self.flogin.errors.append(lazy_gettext(u'Послуга тимчасово недоступна.'))
      return False

    if res['code'] == 1:
      self.flogin.errors.append(lazy_gettext(u'Вичерпано добовий ліміт спроб відновлення пароля через електронну пошту.'))
      return False


    return True


class RecoveryFormSMSOrder(FlaskForm):

  flogin = TextField('Login', [validators.Length(min=1, max=18)])
  phone = TelField('Phone', [validators.DataRequired()])

  def __init__(self, *args, **kwargs):
    FlaskForm.__init__(self, *args, **kwargs)
    self.user = None

  def validate(self):
    rv = FlaskForm.validate(self)
    if not rv:
      return False

    param = { 
              'login' : self.flogin.data,
              'ident' : self.phone.data,
              'key_personal_room' : CABINET_KEY,
              'action': 'sms'
            }

    res = auth.get_cabinet('set_recovery_password_order',param)
    if res['code'] == 3:
      self.flogin.errors.append(lazy_gettext(u'Перевірте правильність введених даних.'))
      return False

    if res['code'] == 2:
      self.flogin.errors.append(lazy_gettext(u'Послуга тимчасово недоступна.'))
      return False

    if res['code'] == 1:
      self.flogin.errors.append(lazy_gettext(u'Вичерпано добовий ліміт спроб відновлення пароля через SMS.'))
      return False

    return True


class RecoveryFormAttempt(FlaskForm):

  fpin = TextField('PIN', [validators.Length(min=3, max=18)])

  newpassword = PasswordField('New Password', [
    validators.Required(),
    validators.Length(min=6, max=25)
    ])

  confirm = PasswordField('Repeat Password')

  def __init__(self, *args, **kwargs):
    FlaskForm.__init__(self, *args, **kwargs)
    self.user = None

  def validate(self):
    rv = FlaskForm.validate(self)
    if not rv:
      return False

    if self.newpassword.data != self.confirm.data:
      self.newpassword.errors.append(lazy_gettext(u'Паролі повинні співпадати.'))
      return False

    param = { 
              'fpin' : self.fpin.data,
              'newpassword' : self.newpassword.data,
              'confirm' : self.confirm.data,
              'key_personal_room' : CABINET_KEY
            }

    res = auth.get_cabinet('set_recovery_password_attempt',param)

    if res['code'] == 0:
      if current_user.is_authenticated:
        session['confirmed'] = True
      flash(gettext(u'Пароль успішно замінено.'),'alert-success')

    if res['code'] == 1:
      self.fpin.errors.append(lazy_gettext(u'Код для підтвердження пароля недійсний, перевірте правильність введення.'))
      return False

    return True
