# -*- coding: utf-8 -*-

from flask import render_template, flash, redirect, request, session, url_for, Response
from app import app
from forms import LoginForm, RecoveryFormMailOrder, RecoveryFormSMSOrder, RecoveryFormAttempt
from forms import NewdeviceForm, ChangepassForm, ContactsForm, PostconfirmForm, WpaymentForm, BonusForm, SMSInfoForm, MailInfoForm
from forms import SrvRealipOnForm, SrvRealipOffForm, ScratchForm 
from forms import SrvDisableForm, SrvEnableForm, SrvChangePacketForm, SrvSheduleSpeedwayForm, SrvCancelSpeedwayForm
from forms import SrvSheduleDisableForm, SrvSheduleEnableForm, SrvShedulePacketForm
from forms import SrvCancelSheduleDisableForm, SrvCancelSheduleEnableForm, SrvCancelShedulePacketForm
from forms import StatisticForm, QuestionCloseForm, QuestionCreateForm
from forms import LastCostsForm, LastPaymentForm
from forms import RememberPasswordBySMSForm, RememberPasswordByMailForm

from forms import FTS_City, FTS_Street, FTS_Build, FTS_Apartment, FTS_ConnectApp

import auth
import json
from flask_login import login_user, logout_user, current_user, login_required
import pprint
from datetime import datetime, date, time
from datetime import timedelta

from flask_babel import gettext
from flask_babel import lazy_gettext

from app import babel
from app import CABINET_VERSION
from functools import wraps
from config import *
from flask import jsonify

from logger import logger

month_dict = { 
    '1': lazy_gettext(u'Січень'), 
    '2': lazy_gettext(u'Лютий'),
    '3': lazy_gettext(u'Березень'),
    '4': lazy_gettext(u'Квітень'),
    '5': lazy_gettext(u'Травень'),
    '6': lazy_gettext(u'Червень'),
    '7': lazy_gettext(u'Липень'),
    '8': lazy_gettext(u'Серпень'),
    '9': lazy_gettext(u'Вересень'),
    '10': lazy_gettext(u'Жовтень'),
    '11': lazy_gettext(u'Листопад'),
    '12': lazy_gettext(u'Грудень')
}


#exit if session is not confirmed
def session_required(f):
  @wraps(f)
  def decorated_function(*args, **kwargs):
    if not session['confirmed']:
      return redirect(url_for('postconfirm', next=request.path))
    return f(*args, **kwargs)
  return decorated_function


#exit if session is not confirmed
def session_num_eq_passwd(f):
  @wraps(f)
  def decorated_function(*args, **kwargs):
    return f(*args, **kwargs)
  return decorated_function


def wcabinet(f):
  @wraps(f)
  def decorated_function(*args, **kwargs):
    param = {'cabinet_key' : CABINET_KEY}
    status=auth.get_cabinet('get_cabinet_status',param)
    if status['code'] == 1:
      return render_template("work_underway_cabinet.html")
    return f(*args, **kwargs)
  return decorated_function


def wconnect(f):
  @wraps(f)
  def decorated_function(*args, **kwargs):
    param = {'connect_key' : CONNECT_KEY}
    status=auth.get_cabinet('get_connect_status',param)
    if status['code'] == 1:
      return render_template("work_underway_connect.html")
    return f(*args, **kwargs)
  return decorated_function


@app.template_filter('tt')
def _jinja2_filter_datetime(time, fmt=None):
  if fmt:
    return time.strftime(fmt)
  else:
    datetime.strptime(time, '%H:%M:%S')
    return datetime.strptime(time, '%H:%M:%S').strftime(gettext('%H:%M'))


@app.template_filter('dt')
def _jinja2_filter_datetime(date, fmt=None):
  if fmt:
    return date.strftime(fmt)
  else:
    datetime.strptime(date, '%Y-%m-%d')
    return datetime.strptime(date, '%Y-%m-%d').strftime(gettext('%d.%m.%Y'))


@babel.localeselector
def get_locale():
  """Визначення і встановлення локалі користувача (мови інтерфейсу).

  Якщо локаль вже була вибрана раніше і збережена в сесії то використовуємо її.
  Інакше, в залежності від налаштувань:
    * або  шукаємо чи бажана локаль з браузера є в списку доступних (автовизначення),
    * або встановлюємо "типову" (стандартну) локаль.

  Returns
  -------
  str
    Повертає назву локалі ('uk', 'ru' тощо).

  """

  # TODO: я так розумію session створюється лише для залогінених користувачів.
  # Тоді треба подумати як пам’ятати мову, що була вибрана ост. разу,
  # навіть якщо зараз користувач не залогінений.

  if 'locale' in session:
    return session['locale']
  else:

    if LOCALE_AUTO:

      for lang in request.accept_languages.values():

        lang_short = lang[:2]   # щоб перетворити різні 'uk_UA', 'ru_RU' в 'uk' і 'ru відповідно'

        if lang_short in LOCALE_LIST:
          return lang_short     # мова клієнта має відповідник в мовах кабінету, встановлюємо її
        else:
          return LOCALE_DEFAULT # в клієнта нема такої мови, яка є в кабінеті, віддаємо стандартну

    else:
      return LOCALE_DEFAULT # встановлюємо типову, нехай клієнт сам міняє, якщо треба

@babel.timezoneselector
def get_timezone():
  if current_user.is_authenticated:
    return user.timezone


# CONNECT ZONE
@app.route('/connect_city', methods = ['GET','POST'])
@wconnect
def connect_city():

  param = {'connect_key' : CONNECT_KEY}
  status=auth.get_cabinet('get_connect_status',param)

  if status['code'] == 0:

    fcity = FTS_City()
    if request.method == 'POST':
      if fcity.validate():

        param = { 
          'master' : status['id_web_localize'] ,
          'query' : fcity.city.data ,
          'mode' : 'fts_city_exact' ,
          'limit' : 1 
        }

        city=auth.get_cabinet('get_ts_address',param)
        if len(city) == 1:
          return redirect(url_for('connect_street', city=city[0]['id']))

    # GET LOCAL STATUS
    is_local = auth.is_local(auth.ip_user())

    return render_template("connect_city.html", 
      fcity = fcity,
      id_web_localize = status['id_web_localize'],
      is_local = is_local
    )

  return render_template("work_underway_connect.html")


@app.route('/connect_street/<city>', methods = ['GET','POST'])
@wconnect
def connect_street(city):

  fstreet = FTS_Street()
  if request.method == 'POST':
    if fstreet.validate():

      param = { 
       'master' : city,
       'query' : fstreet.street.data,
       'mode' : 'fts_street_exact',
       'limit' : 1
       }

      street = auth.get_cabinet('get_ts_address',param)

      if len(street) == 0:
        fstreet.street.errors.append(u'Не знайдено вулицю')

      if len(street) == 1:
        return redirect(url_for('connect_build', street=street[0]['id']))

      if len(street) > 1:
        fstreet.street.errors.append(u'Не знайдено вулицю')

  param = { 
    'id' : city,
    'mode' : 'city'
  }

  address = auth.get_cabinet('get_id_address',param)

  if address['code'] == 1:
    return redirect(url_for('connect_city'))

  # GET LOCAL STATUS
  is_local = auth.is_local(auth.ip_user())

  return render_template("connect_street.html", 
    fstreet = fstreet,
    address = address,
    city = city,
    is_local = is_local
  )


@app.route('/connect_build/<street>', methods = ['GET','POST'])
@wconnect
def connect_build(street):

  skip_quest = False

  fbuild = FTS_Build()
  if request.method == 'POST':
    if fbuild.validate():

      param = { 
       'master' : street ,
       'query' : fbuild.build.data ,
       'mode' : 'fts_build_exact' ,
       'limit' : 1

       }

      build=auth.get_cabinet('get_ts_address',param)

      if len(build) == 0:
        fbuild.build.errors.append(u'Не знайдено номер будинку')
        skip_quest = True

      if len(build) == 1:
        return redirect(url_for('connect_apartment', build=build[0]['id']))

      if len(build) > 1:
        fbuild.build.errors.append(u'Не знайдено номер будинку')

  param = { 
    'id' : street,
    'mode' : 'street'
  }

  address = auth.get_cabinet('get_id_address',param)

  if address['code'] == 1:
    return redirect(url_for('connect_city'))

  # GET LOCAL STATUS
  is_local = auth.is_local(auth.ip_user())

  return render_template("connect_build.html", 
    fbuild = fbuild,
    address = address,
    street = street,
    skip_quest = skip_quest,
    is_local = is_local
  )



@app.route('/connect_apartment/<build>', methods = ['GET','POST'])
@wconnect
def connect_apartment(build):

  skip_quest = False

  fapartment = FTS_Apartment()
  if request.method == 'POST':
    if fapartment.validate():

      param = { 
       'master' : build,
       'query' : fapartment.apartment.data ,
       'mode' : 'fts_apartment_exact' ,
       'limit' : 1
       }

      apartment=auth.get_cabinet('get_ts_address',param)

      if len(apartment) == 0:
        fapartment.apartment.errors.append(u'Не знайдено номер квартири')
        skip_quest = True

      if len(apartment) == 1:
        return redirect(url_for('connect_application', mode='apartment' , id=apartment[0]['id']))

      if len(apartment) > 1:
        fapartment.apartment.errors.append(u'Не знайдено номер квартири')

  param = { 
    'id' : build,
    'mode' : 'build'
  }

  address = auth.get_cabinet('get_id_address',param)

  if address['code'] == 1:
    return redirect(url_for('connect_city'))

  # GET LOCAL STATUS
  is_local = auth.is_local(auth.ip_user())

  return render_template("connect_apartment.html", 
    fapartment = fapartment,
    address = address,
    build = build,
    skip_quest = skip_quest,
    is_local = is_local
  )



@app.route('/connect_application/<mode>/<id>', methods = ['GET','POST'])
@wconnect
def connect_application(mode,id):

  fconnectapp = FTS_ConnectApp()
  if request.method == 'POST':
    if fconnectapp.validate():

      address = fconnectapp.address.data.encode('cp1251', 'ignore').decode('cp1251').encode('utf8')
      name = fconnectapp.name.data.encode('cp1251', 'ignore').decode('cp1251').encode('utf8')
      wishes = fconnectapp.wishes.data.encode('cp1251', 'ignore').decode('cp1251').encode('utf8')

      param = { 
       'mode' : mode,
       'id' : id,
       'address' : address,
       'name' : name,
       'phone' : fconnectapp.phone.data,
       'email' : fconnectapp.email.data,
       'want_internet' : fconnectapp.want_internet.data,
       'want_catv' : fconnectapp.want_catv.data,
       'wishes' : wishes,
       'ip' : auth.ip_user(),
       'cabinet_key': CABINET_KEY
       }

      complete = auth.get_cabinet('set_connect_application',param)

      # if someone try to use unavaliable apartment id
      if complete['code'] == 3:
        return redirect(url_for('connect_application', mode=mode, id=id))

      # GET LOCAL STATUS
      is_local = auth.is_local(auth.ip_user())

      return render_template("connect_complete.html", 
        complete=complete,
        mode = mode,
        id = id,
        is_local = is_local
      )

  param = { 
    'id' : id,
    'mode' : mode
  }

  address = auth.get_cabinet('get_id_address',param)
  if address['code'] == 1:
    return redirect(url_for('connect_city'))

  # GET LOCAL STATUS
  is_local = auth.is_local(auth.ip_user())

  return render_template("connect_application.html", 
    fconnectapp = fconnectapp,
    address = address,
    mode = mode,
    id = id,
    is_local = is_local
  )


@app.route('/connect_lang/<lang>', methods = ['GET', 'POST'])
def connect_lang(lang):
  next = request.args.get('next')
  session['locale'] = lang
  if next:
    return redirect(next)


#CABINET ZONE
@app.route('/')
def base():
  return redirect(url_for('login'))  


@app.route('/cabinet', methods = ['GET','POST'])
@wcabinet
@login_required
def cabinet():


  user = current_user
  base = auth.get_base()

  #exit if number equall password
  if base["credits"]["num_eq_passwd"]:
    return render_template("numeqpass.html", base = base )

  # GET LOCAL STATUS
  ip_user = auth.ip_user()
  is_unknown = auth.is_unknown(ip_user)

  if is_unknown:
    is_unknown_process = auth.is_unknown_process(ip_user)
  else:
    is_unknown_process = False

  param = { 'id_abonent' : user.id_abonent}
  cables=auth.get_cabinet('get_cables',param)
  mainfo=auth.get_cabinet('get_message_for_abonent',param)
  cabinet = []
  return render_template("cabinet.html",
    cabinet = cabinet,
    base = base,
    cables = cables,
    mainfo = mainfo,
    is_unknown = is_unknown,
    is_unknown_process = is_unknown_process
  )

@app.route('/cable', methods = ['GET', 'POST'])
@wcabinet
@login_required
def cable():
  user = current_user
  base = auth.get_base()
  return render_template("cable.html", 
    base = base
  )


@app.route('/get_ts_address/<mode>/<master>/<limit>/<query>', methods = ['GET', 'POST'])
@wcabinet
def get_ts_address(mode,master,limit,query):
  param = { 
   'master' : master, 
   'query' : query,  
   'mode' : mode,  
   'limit' : limit
  }

  result=auth.get_cabinet('get_ts_address',param)
  return Response(json.dumps(result),  mimetype='application/json')


@app.route('/lang/<lang>', methods = ['GET', 'POST'])
@wcabinet
@login_required
def lang(lang):

  user = current_user
  next = request.args.get('next')
  param = { 'id_abonent' : user.id_abonent,
            'locale' : lang
          }
  set_locale=auth.get_cabinet('set_locale',param)
  session['locale'] = lang
  if next:
    return redirect(next)
  else:
    return redirect(url_for('cabinet'))

@app.route('/service/<hash>', methods = ['GET', 'POST'])
@wcabinet
@login_required
def service(hash):

  user = current_user
  base = auth.get_base()
  param = { 'id_abonent' : user.id_abonent,
            'hash' : hash
          }

  service=auth.get_cabinet('get_service',param)

  return render_template("service.html", 
    service = service,
    base = base
  )


@app.route('/statistic/<hash>', methods = ['GET', 'POST'])
@wcabinet
@login_required
def statistic(hash):

  user = current_user
  base = auth.get_base()
  param = { 'id_abonent' : user.id_abonent,
            'hash' : hash
          }

  service=auth.get_cabinet('get_service',param)

  if service["statistic"] == True:
  
    statistic_form = StatisticForm()


    if request.method == 'POST':
      val_combo_box = statistic_form.combo_box.data
    else:
      val_combo_box = 0
      statistic_form.combo_box.data = val_combo_box

    param = { 
              'combo_box' : val_combo_box
            }

    date_combo_boxes=auth.get_cabinet('get_period_date_combo_box',param)
    date_b = date_combo_boxes["date_b"]
    date_e = date_combo_boxes["date_e"]

    param = { 
              'last_months' : 6,
            }

    date_combo_boxes=auth.get_cabinet('date_combo_box',param)

    statistic_form.combo_box.choices = [(date_combo_box["value"], month_dict[str(date_combo_box["month"])]+' '+str(date_combo_box["year"]) ) for date_combo_box in date_combo_boxes ]


    param = { 'id_abonent' : user.id_abonent,
              'hash' : hash,
              'date_b' : date_b,
              'date_e' : date_e
            }

    statistic=auth.get_cabinet('get_statistic',param)

    sum_in = sum( [i['in']   for i in statistic['data'] ] )
    sum_out = sum( [i['out']   for i in statistic['data'] ] )
    sum_money = sum( [i['money']   for i in statistic['data'] ] )

    total = { 
             'sum_in' : sum_in, 
             'sum_out' : sum_out, 
             'sum_money' : sum_money
            }

    cabinet = []

    return render_template("statistic.html", 
      cabinet = cabinet,
      statistic = statistic,
      statistic_form = statistic_form,
      total = total,
      base = base
    )

  return render_template("service.html", 
    service = service,
    base = base
  )

@app.route('/service/<hash>/<action>', methods = ['GET', 'POST'])
@wcabinet
@login_required
@session_required
def service_action(hash, action):

  user = current_user
  base = auth.get_base()
  param = { 'id_abonent' : user.id_abonent,
            'hash' : hash
          }

  service=auth.get_cabinet('get_service',param)

#exit if not permitted
  if base["credits"]["num_eq_passwd"] == True or base["credits"]["manage_tariffs"] == False:
    return render_template("service.html", 
      service = service,
      base = base
    )


# Realip On action
  if action == 'realip_on' and service["action"]["realip"]["on"] == True:
    srv_realip_on = SrvRealipOnForm()
    if request.method == 'POST':
      if srv_realip_on.validate():
        param = { 'id_abonent' : user.id_abonent,
                  'hash' : hash
                }
        srv=auth.get_cabinet('set_service_realip_on',param)
        if srv['code'] == 0:
          flash(gettext(u'Реальна IP адреса успішно замовлена.'),'alert-success')
        if srv['code'] == 1:
          flash(gettext(u'Реальна IP адреса вже замовлена.'),'alert-warning')
        if srv['code'] == 2:
          flash(gettext(u'Недостатньо коштів на рахунку.'),'alert-warning')
        if srv['code'] == 3:
          flash(gettext(u'Немає вільних реальних IP адрес. Спробуйте пізніше.'),'alert-warning')
        if srv['code'] == 4:
          flash(gettext(u'Заміна IP адреси не може відбуватись частіше ніж один раз в хвилину.'),'alert-warning')

        return redirect(url_for('service', hash=hash))

    return render_template("service_realip_on.html", 
      srv_realip_on = srv_realip_on,
      service = service,
      base = base
    )


# Realip Off action
  if action == 'realip_off' and service["action"]["realip"]["off"] == True:
    srv_realip_off = SrvRealipOnForm()
    if request.method == 'POST':
      if srv_realip_off.validate():
        param = { 'id_abonent' : user.id_abonent,
                  'hash' : hash
                }
        srv=auth.get_cabinet('set_service_realip_off',param)
        if srv['code'] == 0:
          flash(gettext(u'Використання реальної IP адреси відмінено.'),'alert-success')
        return redirect(url_for('service', hash=hash))

    return render_template("service_realip_off.html", 
      srv_realip_off = srv_realip_off,
      service = service,
      base = base
    )


# enable action
  if action == 'enable' and service["action"]["enable"] == True:
    srv_enable = SrvEnableForm()
    if request.method == 'POST':
      if srv_enable.validate():
        param = { 'id_abonent' : user.id_abonent,
                  'hash' : hash
                }
        srv=auth.get_cabinet('set_service_enable',param)
        if srv['code'] == 0:
          flash(gettext(u'Послуга успішно увімкнена'),'alert-success')

        return redirect(url_for('service', hash=hash))

    return render_template("service_enable.html", 
      srv_enable = srv_enable,
      service = service,
      base = base
    )


# disable action
  if action == 'disable' and service["action"]["disable"] == True:
    srv_disable = SrvDisableForm()
    if request.method == 'POST':
      if srv_disable.validate():
        param = { 'id_abonent' : user.id_abonent,
                  'hash' : hash
                }
        srv=auth.get_cabinet('set_service_disable',param)
        if srv['code'] == 0:
          flash(gettext(u'Послуга успішно вимкнена'),'alert-success')
        return redirect(url_for('service', hash=hash))

    return render_template("service_disable.html", 
      srv_disable = srv_disable,
      service = service,
      base = base
    )


# shcedule enable switch
  if action == 'schedule_enable' and service["action"]["schedule_enable"] == True:
    srv_schedule_enable = SrvSheduleEnableForm()
    if request.method == 'POST':
      if srv_schedule_enable.validate():
        param = { 'id_abonent' : user.id_abonent,
                  'hash' : hash,
                  'id_active' : 1
                }
        srv=auth.get_cabinet('set_service_schedule_switch',param)
        if srv['code'] == 0:
          flash(gettext(u'Ввімкнення послуги успішно заплановано'),'alert-success')
        return redirect(url_for('service', hash=hash))

    param = { 'id_abonent' : user.id_abonent,
              'hash' : hash
            }
    next_date_active=auth.get_cabinet('get_service_next_date_active',param)

    return render_template("service_schedule_enable.html",
      next_date_active = next_date_active, 
      srv_schedule_enable = srv_schedule_enable,
      service = service,
      base = base
    )


# shcedule disable switch
  if action == 'schedule_disable' and service["action"]["schedule_disable"] == True:
    srv_schedule_disable = SrvSheduleDisableForm()
    if request.method == 'POST':
      if srv_schedule_disable.validate():
        param = { 'id_abonent' : user.id_abonent,
                  'hash' : hash,
                  'id_active' : 0
                }
        srv=auth.get_cabinet('set_service_schedule_switch',param)
        if srv['code'] == 0:
          flash(gettext(u'Вимкнення послуги успішно заплановано'),'alert-success')
        return redirect(url_for('service', hash=hash))

    param = { 'id_abonent' : user.id_abonent,
              'hash' : hash
            }
    next_date_active=auth.get_cabinet('get_service_next_date_active',param)

    return render_template("service_schedule_disable.html", 
      next_date_active = next_date_active, 
      srv_schedule_disable = srv_schedule_disable,
      service = service,
      base = base
    )


# cancel shcedule enable switch
  if action == 'cancel_enable' and service["action"]["cancel_enable"] == True:
    srv_cancel_schedule_enable = SrvCancelSheduleEnableForm()
    if request.method == 'POST':
      if srv_cancel_schedule_enable.validate():
        param = { 'id_abonent' : user.id_abonent,
                  'hash' : hash
                }
        srv=auth.get_cabinet('set_service_cancel_schedule_switch',param)
        if srv['code'] == 0:
          flash(gettext(u'Відміна планування успішно виконана'),'alert-success')
        return redirect(url_for('service', hash=hash))

    return render_template("service_cancel_schedule_enable.html", 
      srv_cancel_schedule_enable = srv_cancel_schedule_enable,
      service = service,
      base = base
    )


# cancel shcedule disable switch
  if action == 'cancel_disable' and service["action"]["cancel_disable"] == True:
    srv_cancel_schedule_disable = SrvCancelSheduleDisableForm()
    if request.method == 'POST':
      if srv_cancel_schedule_disable.validate():
        param = { 'id_abonent' : user.id_abonent,
                  'hash' : hash
                }
        srv=auth.get_cabinet('set_service_cancel_schedule_switch',param)
        if srv['code'] == 0:
          flash(gettext(u'Відміна планування успішно виконана'),'alert-success')
        return redirect(url_for('service', hash=hash))

    return render_template("service_cancel_schedule_disable.html", 
      srv_cancel_schedule_disable = srv_cancel_schedule_disable,
      service = service,
      base = base
    )


# shcedule packet
  if action == 'schedule_packet' and service["action"]["schedule_packet"] == True:

    param = { 'id_abonent' : user.id_abonent, 'hash' : hash }
    # get next_date_packet
    next_date_packet = auth.get_cabinet('get_service_next_date_packet',param)
    #get avaliable packets
    avaliable_packets = auth.get_cabinet('get_avaliable_packets',param)
    srv_schedule_packet = SrvShedulePacketForm()
    srv_schedule_packet.packet.choices = [(packet["id_packet"], packet["name_packet"]) for packet in avaliable_packets["list"] ]
    if request.method == 'POST':
      if srv_schedule_packet.validate():
        param = { 'id_abonent' : user.id_abonent,
                  'hash' : hash, 
                  'id_packet' : srv_schedule_packet.packet.data,
                  'boo_schedule' : True
                }
        srv=auth.get_cabinet('set_avaliable_packets',param)
        if srv['code'] == 0:
          flash(gettext(u'Планування заміни тарифного плану успішно виконано'),'alert-success')
        return redirect(url_for('service', hash=hash))

    not_select = 'Not'
    return render_template("service_schedule_packet.html", 
      not_select = not_select,
      avaliable_packets = avaliable_packets,
      next_date_packet = next_date_packet, 
      srv_schedule_packet = srv_schedule_packet,
      service = service,
      base = base
    )


# shcedule speedway
  if action == 'schedule_speedway' and service["action"]["schedule_speedway"] == True:

    param = { 'id_abonent' : user.id_abonent, 'hash' : hash }
    #get avaliable speedways
    avaliable_speedways = auth.get_cabinet('get_avaliable_speedways',param)
    srv_schedule_speedway = SrvSheduleSpeedwayForm()
    srv_schedule_speedway.speedway.choices = [(speedway["id_packet_speedway"], speedway["name_speedway"]) for speedway in avaliable_speedways["list"] ]

    count_speedway_list = len(avaliable_speedways["list"])

    if request.method == 'POST':
      if srv_schedule_speedway.validate():
        param = { 'id_abonent' : user.id_abonent,
                  'hash' : hash, 
                  'id_packet_speedway' : srv_schedule_speedway.speedway.data
                }
        srv=auth.get_cabinet('set_schedule_speedways',param)
        if srv['code'] == 0:
          flash(gettext(u'Планування Турбо-режиму успішно виконано'),'alert-success')
        return redirect(url_for('service', hash=hash))

    return render_template("service_schedule_speedway.html",
      count_speedway_list = count_speedway_list,
      avaliable_speedways = avaliable_speedways,
      srv_schedule_speedway = srv_schedule_speedway,
      service = service,
      base = base
    )


# cancel shcedule packet
  if action == 'cancel_speedway' and service["action"]["cancel_speedway"] == True:
    srv_cancel_speedway = SrvCancelSpeedwayForm()
    if request.method == 'POST':
      if srv_cancel_speedway.validate():
        param = { 'id_abonent' : user.id_abonent,
                  'hash' : hash
                }
        srv=auth.get_cabinet('set_cancel_speedways',param)
        if srv['code'] == 0:
          flash(gettext(u'Відміна планування успішно виконана'),'alert-success')
        return redirect(url_for('service', hash=hash))

    return render_template("service_cancel_speedway.html", 
      srv_cancel_speedway = srv_cancel_speedway,
      service = service,
      base = base
    )


# cancel shcedule packet
  if action == 'cancel_packet' and service["action"]["cancel_packet"] == True:
    srv_cancel_schedule_packet = SrvCancelShedulePacketForm()
    if request.method == 'POST':
      if srv_cancel_schedule_packet.validate():
        param = { 'id_abonent' : user.id_abonent,
                  'hash' : hash
                }
        srv=auth.get_cabinet('set_service_cancel_schedule_packet',param)
        if srv['code'] == 0:
          flash(gettext(u'Відміна планування успішно виконана'),'alert-success')
        return redirect(url_for('service', hash=hash))

    return render_template("service_cancel_schedule_packet.html", 
      srv_cancel_schedule_packet = srv_cancel_schedule_packet,
      service = service,
      base = base
    )


# change packet
  if action == 'change_packet' and service["action"]["change_packet"] == True:

    param = { 'id_abonent' : user.id_abonent, 'hash' : hash }
    #get avaliable packets
    avaliable_packets = auth.get_cabinet('get_avaliable_packets',param)
    srv_change_packet = SrvChangePacketForm()
    srv_change_packet.packet.choices = [(packet["id_packet"], packet["name_packet"]) for packet in avaliable_packets["list"] ]
    if request.method == 'POST':
      if srv_change_packet.validate():
        param = { 'id_abonent' : user.id_abonent,
                  'hash' : hash, 
                  'id_packet' : srv_change_packet.packet.data,
                  'boo_schedule' : False
                }
        srv=auth.get_cabinet('set_avaliable_packets',param)
        if srv['code'] == 0:
          flash(gettext(u'Заміна тарифного плану успішно виконана'),'alert-success')
        return redirect(url_for('service', hash=hash))

    return render_template("service_change_packet.html", 
      avaliable_packets = avaliable_packets,
      srv_change_packet = srv_change_packet,
      service = service,
      base = base
    )

# OTTv1
  if action == 'ottv1' and service["action"]["ottv1"] == True:
    param = { 'id_abonent' : user.id_abonent, 'hash' : hash }
    #get ottv1 records
    get_ottv1 = auth.get_cabinet('get_ottv1',param)
    return render_template("service_ottv1.html", 
      get_ottv1 = get_ottv1,
      service = service,
      base = base
    )


# OTTv2
  if action == 'ottv2' and service["action"]["ottv2"] == True:
    param = { 'id_abonent' : user.id_abonent, 'hash' : hash }
    #get ottv2 records
    get_ottv2 = auth.get_cabinet('get_ottv2',param)
    return render_template("service_ottv2.html", 
      get_ottv2 = get_ottv2,
      service = service,
      base = base
    )


  return render_template("service.html", 
    service = service,
    base = base
  )


@app.route('/bonus/<int:mode>', methods = ['GET', 'POST'])
@wcabinet
@login_required
@session_required
def bonus(mode):

  user = current_user
  base = auth.get_base()

  if base["credits"]["num_eq_passwd"] == True or base["credits"]["bonus"]["allow"] == False:
    return redirect(url_for('cabinet'))    

  bonus = BonusForm()
  if request.method == 'POST':
    if bonus.validate():
      return redirect(url_for('bonus',mode=mode))

  else:

    # Get Auto Bonus parameters
    bonus.auto.data = base["credits"]["auto_bonus_ab"]
    auto_bonus_allow = base["credits"]["auto_bonus"]["allow"]

    if mode == 2:
      param = {
                'id_operator' : base["credits"]["id_operator"],
                'id_contract' : base["credits"]["bonus"]["id_contract"]
              }
      contract = auth.get_cabinet('bonus_contract',param)
    else:
      contract = {'code' : 0}

    return render_template("bonus.html", 
      contract = contract,
      auto_bonus_allow = auto_bonus_allow,
      bonus = bonus,
      base = base
    )


@app.route('/wpayment/<int:mode>', methods = ['GET', 'POST'])
@wcabinet
@login_required
@session_required
def wpayment(mode):

  user = current_user
  base = auth.get_base()

  if base["credits"]["num_eq_passwd"] == True or base["credits"]["wait_payment"]["allow"] == False:
    return redirect(url_for('cabinet'))    

  wpayment = WpaymentForm()
  if request.method == 'POST':
    if wpayment.validate():
      return redirect(url_for('cabinet'))

  # Get Wait For Payment parameters
  param = { 'id_abonent' : user.id_abonent,
            'only_check' : True
          }
  wpayment_info=auth.get_cabinet('wpayment',param)

  if mode == 2:
    param = { 'id_abonent' : user.id_abonent,
              'id_operator' : base["credits"]["id_operator"],
              'id_contract' : base["credits"]["wait_payment"]["id_contract"]
            }
    contract = auth.get_cabinet('wpayment_contract',param)

    contract = {'code' : 1, 'message': contract["message"]}
  else:
    contract = {'code' : 0}

  return render_template("wpayment.html", 
    contract = contract,
    wpayment_info = wpayment_info,
    wpayment = wpayment,
    base = base
  )


@app.route('/contacts', methods = ['GET', 'POST'])
@wcabinet
@login_required
@session_required
def contacts():

  user = current_user
  base = auth.get_base()

  # Begin Personal section
  show_header_personal_data = False
  show_date_birth = False

  if base["credits"]["num_eq_passwd"] == True or base["credits"]["set_contacts"]["allow"] == False:
    return redirect(url_for('cabinet'))    

  contacts = ContactsForm()
  if request.method == 'POST':
    if contacts.validate():
      return redirect(url_for('contacts'))
  else:
    contacts.phone1.data = base["credits"]["phone1"]
    contacts.phone2.data = base["credits"]["phone2"]
    contacts.email.data = base["credits"]["email"]
    contacts.callback.data = base["credits"]["callback"]

    # Date birth section
    param = { 
              'id_abonent' : user.id_abonent,
              'cod_typ_abonent_storage' : 1
            }
    abonent_storage = auth.get_cabinet('get_abonent_storage',param)
    if abonent_storage["data"]:
      d = datetime.strptime(abonent_storage["data"], '%Y-%m-%d')
      contacts.date_birth.data = date.strftime(d, gettext('%d.%m.%Y'))

    if abonent_storage["cod_typ_abonent_storage"]:
      show_date_birth = True
      show_header_personal_data = True


    # End Personal section

  return render_template("contacts.html", 
    contacts = contacts,
    show_header_personal_data = show_header_personal_data,
    show_date_birth = show_date_birth,
    base = base
  )


@app.route('/remember_password', methods = ['GET', 'POST'])
@wcabinet
@login_required
def remember_password():

  user = current_user
  param = { 'id_abonent' : user.id_abonent }
  remember_password = auth.get_cabinet('get_remember_password_status',param)

  base = auth.get_base()
  return render_template("remember_password.html", 
    remember_password = remember_password,
    base = base
  )


@app.route('/remember_by/<action>', methods = ['GET', 'POST'])
@wcabinet
@login_required
def remember_by(action):

  user = current_user
  param = { 'id_abonent' : user.id_abonent }
  remember_password = auth.get_cabinet('get_remember_password_status',param)
  base = auth.get_base()

# SMS
  if action == 'sms':
    if remember_password["code"] == 0:
      if remember_password["sms"]["code"] == 0:

        by_sms = RememberPasswordBySMSForm()
        if request.method == 'POST':
          if by_sms.validate():

            ident = remember_password["sms"]["number"]
            num_abonent = base["credits"]["num_abonent"]

            param = { 
                      'login' : num_abonent,
                      'ident' : ident,
                      'key_personal_room' : CABINET_KEY,
                      'action': action
                    }

            res = auth.get_cabinet('set_recovery_password_order',param)

            if res['code'] == 0:
              return redirect(url_for('recovery_password', wizard='attempt', action=action))

            if res['code'] == 3:
              flash(gettext(u'Перевірте правильність введених даних.'),'alert-warning')

            if res['code'] == 2:
              flash(gettext(u'Послуга тимчасово недоступна.'),'alert-warning')

            if res['code'] == 1:
              flash(gettext(u'Вичерпано добовий ліміт спроб відновлення пароля через SMS.'),'alert-warning')

        else:
          return render_template("remember_password_by_sms.html", 
                  remember_password = remember_password,
                  by_sms = by_sms,
                  base = base
                 )


# E-mail
  if action == 'mail':
    if remember_password["code"] == 0:
      if remember_password["mail"]["code"] == 0:

        by_mail = RememberPasswordByMailForm()
        if request.method == 'POST':
          if by_mail.validate():

            ident = remember_password["mail"]["box"]
            num_abonent = base["credits"]["num_abonent"]

            param = { 
                      'login' : num_abonent,
                      'ident' : ident,
                      'key_personal_room' : CABINET_KEY,
                      'action': action
                    }

            res = auth.get_cabinet('set_recovery_password_order',param)

            if res['code'] == 0:
              return redirect(url_for('recovery_password', wizard='attempt', action=action))

            if res['code'] == 3:
              flash(gettext(u'Перевірте правильність введених даних.'),'alert-warning')

            if res['code'] == 2:
              flash(gettext(u'Послуга тимчасово недоступна.'),'alert-warning')

            if res['code'] == 1:
              flash(gettext(u'Вичерпано добовий ліміт спроб відновлення пароля через електронну пошту.'),'alert-warning')

        else:
          return render_template("remember_password_by_mail.html", 
                 remember_password = remember_password,
                 by_mail = by_mail,
                 base = base
                 )

  return redirect(url_for('remember_password'))


@app.route('/speed_info', methods = ['GET', 'POST'])
@wcabinet
@login_required
@session_required
def speed_info():

  user = current_user
  speed_info = []
  base = auth.get_base()

  if base["credits"]["num_eq_passwd"] == True or (base["credits"]["sms_info"]["allow"] == False and base["credits"]["mail_info"]["allow"] == False):
    return redirect(url_for('cabinet'))

  return render_template("speed_info.html", 
    speed_info = speed_info,
    base = base
  )


@app.route('/speed_mail_info', methods = ['GET', 'POST'])
@wcabinet
@login_required
def speed_mail_info():

  speed_info = []
  base = auth.get_base()

  if base["credits"]["num_eq_passwd"] == True or base["credits"]["mail_info"]["allow"] == False:
    return redirect(url_for('cabinet'))

  speed_mail_info = MailInfoForm()
  if request.method == 'POST':
    if speed_mail_info.validate():
      return redirect(url_for('speed_mail_info'))
  else:
    speed_mail_info.mail.data=base["credits"]["mail_info"]["value"]

  return render_template("speed_mail_info.html", 
    speed_info = speed_info,
    speed_mail_info = speed_mail_info,
    base = base
  )


@app.route('/speed_sms_info', methods = ['GET', 'POST'])
@wcabinet
@login_required
def speed_sms_info():

  speed_info = []
  base = auth.get_base()

  if base["credits"]["num_eq_passwd"] == True or base["credits"]["sms_info"]["allow"] == False:
    return redirect(url_for('cabinet'))

  speed_sms_info = SMSInfoForm()
  if request.method == 'POST':
    if speed_sms_info.validate():
      return redirect(url_for('speed_sms_info'))
  else:
    speed_sms_info.sms.data=base["credits"]["sms_info"]["value"]

  return render_template("speed_sms_info.html", 
    speed_info = speed_info,
    speed_sms_info = speed_sms_info,
    base = base
  )


@app.route('/last_payments', methods = ['GET', 'POST'])
@wcabinet
@login_required
@session_required
def last_payments():

  user = current_user
  base = auth.get_base()
  if base["credits"]["num_eq_passwd"] == True or base["credits"]["last_payments"]["allow"] == False:
    return redirect(url_for('cabinet'))

  last_payments = LastPaymentForm()

  if request.method == 'POST':
    val_combo_box = last_payments.combo_box.data
  else:
    val_combo_box = 0
    last_payments.combo_box.data = val_combo_box

  param = { 
            'combo_box' : val_combo_box
          }

  date_combo_boxes=auth.get_cabinet('get_period_date_combo_box',param)
  date_b = date_combo_boxes["date_b"]
  date_e = date_combo_boxes["date_e"]

  param = { 
            'last_months' : 6,
          }

  date_combo_boxes=auth.get_cabinet('date_combo_box',param)

  last_payments.combo_box.choices = [(date_combo_box["value"], month_dict[str(date_combo_box["month"])]+' '+str(date_combo_box["year"]) ) for date_combo_box in date_combo_boxes ]

  param = { 
            'id_abonent' : user.id_abonent,
            'date_b' : date_b,
            'date_e' : date_e
          }

  payments=auth.get_cabinet('last_payments',param)

  sum_payments = sum( [i['payment']   for i in payments['data'] ] )

  return render_template("last_payments.html", 
    last_payments = last_payments,
    payments = payments,
    sum_payments = sum_payments,
    base = base
  )



@app.route('/last_costs', methods = ['GET', 'POST'])
@wcabinet
@login_required
@session_required
def last_costs():

  user = current_user

  base = auth.get_base()

  if base["credits"]["num_eq_passwd"] == True or base["credits"]["last_costs"]["allow"] == False:
    return redirect(url_for('cabinet'))    

  last_costs = LastCostsForm()
  if request.method == 'POST':
    val_combo_box = last_costs.combo_box.data
  else:
    val_combo_box = 0
    last_costs.combo_box.data = val_combo_box

  param = { 
            'combo_box' : val_combo_box
          }

  date_combo_boxes=auth.get_cabinet('get_period_date_combo_box',param)
  date_b = date_combo_boxes["date_b"]
  date_e = date_combo_boxes["date_e"]

  param = { 
            'last_months' : 6,
          }

  date_combo_boxes=auth.get_cabinet('date_combo_box',param)

  last_costs.combo_box.choices = [(date_combo_box["value"], month_dict[str(date_combo_box["month"])]+' '+str(date_combo_box["year"]) ) for date_combo_box in date_combo_boxes ]

  param = { 'id_abonent' : user.id_abonent,
            'date_b' : date_b,
            'date_e' : date_e
          }

  costs=auth.get_cabinet('last_costs',param)

  sum_cables = sum( [i['cost']   for i in costs['cables'] ] )
  sum_months = sum( [i['cost']   for i in costs['months'] ] )
  sum_days = sum( [i['cost']   for i in costs['days'] ] )
  sum_others = sum( [i['cost']   for i in costs['others'] ] )

  sum_total = sum_cables + sum_months + sum_days + sum_others

  return render_template("last_costs.html", 
    last_costs = last_costs,
    costs = costs,
    base = base,
    sum_cables = sum_cables,
    sum_months = sum_months,
    sum_days = sum_days,
    sum_others = sum_others,
    sum_total = sum_total
  )



@app.route('/newdevice', methods = ['GET', 'POST'])
@wcabinet
@login_required
def newdevice():

  user = current_user
  base = auth.get_base()

  newdevice = NewdeviceForm()
  if request.method == 'POST':
    if newdevice.validate():
      return redirect(url_for('cabinet'))

  param = { 
            'id_abonent' : user.id_abonent,
            'ip_address' : auth.ip_user()
          }

  status_unknown_action = auth.get_cabinet('get_status_unknown_action',param)

  return render_template("newdevice.html", 
    newdevice = newdevice,
    base = base,
    status_unknown_action = status_unknown_action
  )



@app.route('/changepass', methods = ['GET', 'POST'])
@wcabinet
@login_required
def changepass():

  base = auth.get_base()

  #exit if number equall password
  if not base["credits"]["num_eq_passwd"]:
    if not session['confirmed']:
      return redirect(url_for('postconfirm', next=request.path))


  if base["credits"]["change_password"]["allow"] == False:
    return redirect(url_for('cabinet'))    

  changepass = ChangepassForm()
  if request.method == 'POST':
    if changepass.validate():
      return redirect(url_for('cabinet'))
  else:
    changepass.newpassword.data = None
    changepass.confirm.data = None

  return render_template("changepass.html", 
    changepass = changepass,
    base = base
  )


@app.route('/scratch', methods = ['GET', 'POST'])
@wcabinet
@login_required
def scratch():

  base = auth.get_base()

  #exit if number equall password
  if not base["credits"]["num_eq_passwd"]:
    if not session['confirmed']:
      return redirect(url_for('postconfirm', next=request.path))


  if base["credits"]["change_password"]["allow"] == False:
    return redirect(url_for('cabinet'))    

  scratch = ScratchForm()
  if request.method == 'POST':
    if scratch.validate():
      return redirect(url_for('cabinet'))
  else:
    scratch.passwd.data = None

  return render_template("scratch.html", 
    scratch = scratch,
    base = base
  )


@app.route('/postconfirm', methods = ['GET', 'POST'])
@wcabinet
@login_required
def postconfirm():

  next = request.args.get('next')
  base = auth.get_base()
  postconfirm = PostconfirmForm()
  if request.method == 'POST':
    if postconfirm.validate():

      if next:
        return redirect(next)
      else:
        return redirect(url_for('cabinet'))

  return render_template("postconfirm.html", 
    postconfirm = postconfirm,
    base = base
  )


@app.route('/funds')
@wcabinet
@login_required
@session_num_eq_passwd
def funds(base=None):

  user = current_user
  base = auth.get_base()
  param = { 'id_abonent' : user.id_abonent,
            'key_personal_room' : CABINET_KEY
          }

  funds=auth.get_cabinet('get_funds',param)
  return render_template("funds.html", 
    funds = funds,
    base = base
  )


@app.route('/funds_terminal/<terminal>', methods = ['GET', 'POST'])
@wcabinet
@login_required
def funds_terminal(terminal):

  user = current_user
  base = auth.get_base()

  if request.method == 'POST':

    amount = request.form.get('amount')
    name_abonent = request.form.get('name_abonent')
    num_abonent = request.form.get('num_abonent')
    address = request.form.get('address')
    city = request.form.get('city')
    id_bank = request.form.get('id_bank')
    service_code = request.form.get('service_code')

    param = { 
              'terminal': terminal,
              'id_bank': id_bank,
              'num_abonent': num_abonent,
              'amount': amount,
              'service_code': service_code
            }

    funds_terminal=auth.get_cabinet('get_funds_terminal',param)

    return render_template("funds_terminal.html", 
      terminal = terminal,
      amount = amount,
      name_abonent = name_abonent,
      num_abonent = num_abonent,
      address = address,
      city = city,
      id_bank = id_bank,
      funds_terminal = funds_terminal,
      base = base
    )

  return redirect(url_for('funds'))



@app.route('/question_archive/<page>', methods = ['GET', 'POST'])
@wcabinet
@login_required
@session_required
def question_archive(page):

  user = current_user
  base = auth.get_base()

  param = { 
            'id_abonent' : user.id_abonent,
            'page': page
          }

  question_archive = auth.get_cabinet('get_question_archive',param)
  question=[]

  return render_template("question_archive.html",
    page = page,
    question_archive = question_archive,
    question = question,
    base = base
  )


@app.route('/question', methods = ['GET', 'POST'])
@wcabinet
@login_required
@session_required
def question():

  user = current_user
  base = auth.get_base()

  question_close_form = QuestionCloseForm()
  question_create_form = QuestionCreateForm()

  param = { 'id_abonent' : user.id_abonent }

  question = auth.get_cabinet('get_question',param)
  action = question["action"]

  if action == 1:
    if request.method == 'POST':
      if question_create_form.validate():
        quest = question_create_form.quest.data.encode('cp1251', 'ignore').decode('cp1251').encode('utf8')
        param = { 
                  'id_abonent' : user.id_abonent,
                  'phone' : question_create_form.phone.data,
                  'email' : question_create_form.email.data,
                  'quest' : quest
                }

        set_question_form = auth.get_cabinet('set_question_form',param)
        question = auth.get_cabinet('get_question',param)
        action = question["action"]


  if action == 3:
    if request.method == 'POST':
      if question_close_form.validate():
        set_question = auth.get_cabinet('set_question',param)
        question = auth.get_cabinet('get_question',param)
        action = question["action"]


  return render_template("question.html",
    action = action,
    question_close_form = question_close_form,
    question_create_form = question_create_form,
    question = question,
    base = base
  )

#@app.route('/')
@app.route('/check')
@wcabinet
def check():

  param = {'cabinet_key' : CABINET_KEY}
  status=auth.get_cabinet('get_cabinet_status',param)

  if status['without_auth']:

    next = request.args.get('next')
    param = { 
              'ip_user': auth.ip_user(),
              'login': '',
              'password': '',
              'cert': '',
              'auth': TYPE_AUTH,
              'key': CABINET_KEY 
            }
    user = auth.get_cabinet('get_user',param)

    if user:

      if next:
        return redirect(next)
      else:
        return redirect(url_for('cabinet'))
    else:
      return redirect(url_for('cabinet'))

  else:
    return redirect(url_for('logout'))



@app.route('/cert', methods = ['GET', 'POST'])
@wcabinet
def cert():
  next = request.args.get('next')
  if request.method == 'GET':
    param = { 
              'ip_user': auth.ip_user(),
              'login': '',
              'password': '',
              'cert': request.args.get('cert'),
              'auth': TYPE_AUTH,
              'key': CABINET_KEY 
            }
    user = auth.get_cabinet('get_user',param)
    if user:
      if next:
        return redirect(next)
      else:
        return redirect(url_for('cabinet'))

  return redirect(url_for('cabinet'))


@app.route('/login', methods = ['GET', 'POST'])
@wcabinet
def login():

  if current_user.is_authenticated:
    return redirect(url_for('cabinet'))
  next = request.args.get('next')
  form = LoginForm()
  if request.method == 'POST' and form.validate():
   if next:
     return redirect(next)
   else:
     return redirect(url_for('cabinet'))
  else:

    # GET LOCAL STATUS
    is_local = auth.is_local(auth.ip_user())    

    # GET CABINET STATUS
    param = {'cabinet_key' : CABINET_KEY}
    status=auth.get_cabinet('get_cabinet_status',param)
    recovery_password_mail = status['recovery_password_mail']
    recovery_password_sms = status['recovery_password_sms']
    without_auth  = status['without_auth']

    return render_template('login.html', 
      form = form,
      is_local = is_local,
      recovery_password_mail = recovery_password_mail,
      recovery_password_sms = recovery_password_sms,
      without_auth = without_auth,
      type_auth = TYPE_AUTH
      )


@app.route('/logout')
def logout():
  auth.ulogger('debug','LOGOUT')
  logout_user()
  session.pop('id_abonent', None)
  session.pop('confirmed', None)
  session.pop('locale', None)
  return redirect(url_for('login'))


@app.route('/recovery_password_form', methods = ['GET', 'POST'])
@wcabinet
def recovery_password_form():

  if not current_user.is_authenticated:

    # GET CABINET STATUS
    param = {'cabinet_key' : CABINET_KEY}
    status=auth.get_cabinet('get_cabinet_status',param)
    recovery_password_mail = status['recovery_password_mail']
    recovery_password_sms = status['recovery_password_sms']

    return render_template('recovery_password_form.html',
      recovery_password_mail = recovery_password_mail,
      recovery_password_sms = recovery_password_sms
      )

  else:
    return redirect(url_for('logout'))


@app.route('/recovery_password/<wizard>/<action>', methods = ['GET', 'POST'])
@wcabinet
def recovery_password(wizard,action):


  # GET CABINET STATUS
  if current_user.is_authenticated:
    user = current_user
    param = { 'id_abonent' : user.id_abonent }
    status = auth.get_cabinet('get_remember_password_status',param)
    if status["code"] == 0:
      if action == 'mail':
        if status["mail"]["code"] == 0:
          recovery_password_mail = True
      if action == 'sms':
        if status["sms"]["code"] == 0:
          recovery_password_sms = True

  else:
    param = {'cabinet_key' : CABINET_KEY}
    status=auth.get_cabinet('get_cabinet_status',param)
    recovery_password_mail = status['recovery_password_mail']
    recovery_password_sms = status['recovery_password_sms']


  # ORDER SECTION
  if wizard == 'order':

    # MAIL SECTION
    if recovery_password_mail and action == 'mail':

      form = RecoveryFormMailOrder()

      if request.method == 'POST' and form.validate():
        return redirect(url_for('recovery_password', wizard='attempt', action=action))
      else:
        return render_template('recovery_password_mail.html',
        form = form
        )

    # SMS SECTION
    if recovery_password_sms and action == 'sms':

      form = RecoveryFormSMSOrder()

      if request.method == 'POST' and form.validate():
        return redirect(url_for('recovery_password', wizard='attempt', action=action))
      else:
        return render_template('recovery_password_sms.html',
        form = form
        )


  # ORDER SECTION
  if wizard == 'attempt':

    # MAIL SECTION
  #  if recovery_password_mail:

    form = RecoveryFormAttempt()
    if request.method == 'POST' and form.validate():
      return redirect(url_for('login'))
    else:
      return render_template('recovery_password_attempt.html',
        form = form,
        action = action  
        )


  return redirect(url_for('logout'))



@app.route('/channels/<technology>/<type_tv>/<code_tv>', methods = ['GET', 'POST'])
@wcabinet
def channels(technology,type_tv,code_tv):
  param = {
    'technology' : technology,
    'cabinet_key' : CABINET_KEY,
    'code_tv' : code_tv
    }

  channel=auth.get_cabinet('get_channels',param)

  if type_tv == 'json':
    resp = Response(json.dumps(channel),  mimetype='application/json')
    return(resp)

  if type_tv == 'html':

    if technology == 'analog-tv': 
      return render_template("channels_analog_tv.html", 
        channel = channel,
        count = len(channel['list'])
        )

    if technology == 'dvb-c': 
      return render_template("channels_dvb_c.html", 
        channel = channel,
        count = len(channel['list'])
        )

@app.route('/about/')
def about():
    """ Сторінка, що відображає відомості про кабінет,
    параметри відвідувача та його оточення, у вигляді параметрів та значень.

    Returns
    -------
    dict
        Видає шаблон, в який передається словник, що містить:
            порядковий номер параметра,
            назву параметра,
            значення параметра.

        Наприклад:  {
                        '1' : {'key1' : 'value1'},
                        '2' : {'key2' : 'value2'}
                    }

        Порядковий номер введено для того, щоб можна було сортувати параметри
        і виводити їх у бажаному порядку, а також щоб можна було їх тематично групувати.
    """

    # TODO
    # фактично, один порядковий номер може містити кілька пар ключ-значення.
    # Це можна використати у майбутньому.

    params = {}

    params['10']  = {lazy_gettext(u'IP-адреса користувача')     : auth.ip_user()}
    params['11']  = {lazy_gettext(u'Авторизований користувач')  : lazy_gettext(u'Так') if current_user.is_authenticated else lazy_gettext(u'Ні')}

    params['20']  = {lazy_gettext(u'Веб-оглядач')               : request.headers.get('User-Agent')}
    params['21']  = {lazy_gettext(u'Бажана локаль')             : request.accept_languages.values()}
    params['22']  = {lazy_gettext(u'Поточна локаль')            : get_locale()}

    params['90']  = {lazy_gettext(u'Кабінет')                   : ALIAS}
    params['91']  = {lazy_gettext(u'Версія API')                : VIVA_API}
    params['99']  = {lazy_gettext(u'Версія кабінету')           : CABINET_VERSION}

    return render_template("about.html", params=params)