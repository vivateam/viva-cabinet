# -*- coding: utf-8 -*

from flask import Flask, session
from flask import render_template, redirect, request, url_for
import warnings
import ssl
import json
import requests
import socket
import binascii
from requests.auth import HTTPBasicAuth
from config import *
from flask_login import login_user, logout_user, current_user, login_required
from logger import logger

class User(object):
  def __init__(self, id_abonent, id_service=None, confirmed=False, locale=None):
    self.id_abonent = id_abonent
    self.id_service = id_service
    self.confirmed = confirmed
    self.locale = locale

  def is_authenticated(self):
    return True
  def is_active(self):
    return True
  def is_anonymous(self):
    return False
  def get_id(self):
    return unicode(self.id_abonent)

def ip_user():

  # SYM, 05.09.16
  # Степан, я закоментував цей код, бо він не працює, якщо NGINX взаємодіє з Flask (Gunikorn)
  # через сокет, а не через IP.
  # Більш універсальний варіант я пропоную нижче.

  #trusted_proxies = {'127.0.0.1'}
  #route = request.access_route + [request.remote_addr]
  #remote_addr = next((addr for addr in reversed(route)
    #if addr not in trusted_proxies), request.remote_addr)
#  remote_addr='10.120.7.18'
  #return remote_addr


  # SYM, 05.09.16
  # Проблема: щоб взнати IP-адресу користувача можна використовувати request.remote_addr.
  # Але! Якщо доступ до Flask (Gunikorn), відбувається через NGINX,
  # то IP-адресою того, хто робить запит буде IP-адреса NGINX.
  # Більше того. Якщо NGINX працює з Flask (Gunikorn) через сокет, а не IP,
  # то IP-адреса ініціатора з’єднання (request.remote_addr) буде порожньою.
  #
  # Тоді, здавалося б, можна використовувати HTTP-заголовок "X-Forwarded-For" (request.access_route),
  # в якому проміжні сервери мали б передавати IP-адресу клієнта, який звернувся до проксі.
  # Але! HTTP заголовок "X-Forwarded-For" може містити IP-адреси усіх проміжних проксі-сервіерів,
  # ба навіть адреси, які вставить в заголовок сам користувач (емулюючи прохід через свій внутрішній проксі).
  # Наприклад curl -H "X-Forwarded-For: 1.2.3.4" "http://new.my.telelan.com.ua/"
  # Таким чином, заголовку "X-Forwarded-For" довіряти не можна!
  #
  # !!! Тому, обов’язково, в налаштування NGINX потрібно прописати директиву
  # proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
  # таким чином кінцева IP-адреса ініціатора, буде вставлятися (дописуватися) 
  # нашим NGINX в кінець заголовку "X-Forwarded-For",
  # і навіть при зміні заголовку  на стороні користувача він буде таким:
  # "X-Forwarded-For: 1.2.3.4, 193.53.83.30", де "193.53.83.30" — IP-адреса користувача в інтернеті.
  #
  # Тому, якщо є заголовок "X-Forwarded-For" і наш NGINX налаштований правильно,
  # ми сміливо можемо брати останній елемент зі списку IP що є в заголовку.
  # Якщо заголовок не встановлено, значить Flask (Gunicorn) працює з клієнтом напряму, 
  # а не через якийсь NGINX і IP-адреса клієнта буде у змінній request.remote_addr.
  #
  # P.S. http://esd.io/blog/flask-apps-heroku-real-ip-spoofing.html
  if DEBUG_IP_ENABLED:
    return DEBUG_IP_NAME

  if request.headers.getlist("X-Forwarded-For"):
    forwarded_for = request.headers.getlist("X-Forwarded-For")[0]
    user_ip = forwarded_for.split(',')[-1].strip()
    ulogger('debug','Get IP %s from X-Forwarded-For' %user_ip)
  else:
    user_ip = request.remote_addr
    ulogger('debug','Get IP %s from Remote-Addr' %user_ip)

  return user_ip


def get_cabinet(func,param):
  
  ulogger('debug','func %s' %func)
  warnings.filterwarnings("ignore")
  params = {'func': func, 'param': param, 'api': '1'}
  auth = HTTPBasicAuth(VIVA_USERNAME, VIVA_PASSWORD)
  r = requests.post(VIVA_URL+VIVA_PATH, json=params, auth=auth, verify=VIVA_CERT)
  if r.status_code == 200:
    data = r.json()
    if func == "get_user":
      if data["user"]:
        if data["user"]["id_abonent"]:
          id_abonent = data["user"]["id_abonent"]
          confirmed = data["user"]["confirmed"]
          locale = data["user"]["locale"]
          edata = User( id_abonent = id_abonent, confirmed = confirmed, locale = locale )
          login_user(edata)
          session['id_abonent'] = id_abonent
          session['confirmed'] = confirmed
          session['locale'] = locale
          ulogger('debug','LOGIN confirmed %s locale %s' %(confirmed,locale))
        else:
          edata = None  
      else:
        edata = None
    else:
      edata = data
  else:
    edata = None
    ulogger('error','func %s, status code %d, params %s' %(func, r.status_code, param))

  return edata


def get_base():
  param = { 'id_abonent': current_user.id_abonent }
  credits = get_cabinet('get_credits', param)
  return { 'credits' : credits }


def get_lang():
  if 'locale' in session:
    return session['locale']
  else:
    return 'en'

def is_local(ip_address):
  param = { 'ip_address': ip_address }
  status = get_cabinet('get_status_local_ip',param)
  return status['status']

def is_unknown(ip_address):
  param = { 'ip_address': ip_address }
  status = get_cabinet('get_status_unknown_ip',param)
  return status['status']

def is_unknown_process(ip_address):
  param = { 'ip_address': ip_address }
  status = get_cabinet('get_status_unknown_ip_process',param)
  return status['status']

def ulogger(level,message):

  if current_user.is_authenticated:
    mess = '[ID-%s] %s' %(current_user.id_abonent, message)
  else:
    mess = '[NOAUTH] %s' %message

  if level == 'debug':
    logger.debug(mess)

  if level == 'error':
    logger.error(mess)
